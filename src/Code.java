import java.util.Random;

public class Code {
	static char[] farben = {'r','g','b','v','w','s'};
	int[] code = new int[4];
			
	public Code() {
		Random zufall = new Random();
		for (int i=0; i<code.length; i++) {
			this.code[i] = zufall.nextInt(5);
		}
	}
	
	 public String versuch(String eingabe) {
		 char[] eingabeArray = new char[4];
		 char[] codeCopy = new char[4];
		 
		 for (int i = 0; i < code.length; i++) {
			codeCopy[i] = farben[code[i]];
		}
		 
		 for (int i = 0; i < eingabe.length(); i++) {
			eingabeArray[i] = eingabe.charAt(i);
		}
		 
		 String antwort = "";
		 for (int i = 0; i < eingabeArray.length; i++) {
			if (eingabeArray[i] == codeCopy[i]) {
				antwort += "B ";
				eingabeArray[i] = ' ';
				codeCopy[i] = ' ';
			}
			
		}
		 for (int i = 0; i < eingabeArray.length; i++) {
			 for (int j = 0; j < code.length; j++) {

					if (eingabeArray[i] != ' ') {
						if (eingabeArray[i] == codeCopy[j]) {
							antwort += "W ";
							eingabeArray[i] = ' ';
							codeCopy[i] = ' ';
						}
					}
			}
		}

		System.out.println("Code check: " + antwort);
		return antwort;
	}
	
	public boolean gewonnen(String antwort) {
		if(antwort.equals("B B B B ") ) {
			System.out.println("WIN");
			return true;
		}
		
		return false;
	}
	public static boolean gültigeFarbe(char f) {
		for (int j = 0; j < farben.length; j++) {
			if (f == farben[j]) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean gültigeFarbe(String f) {
	
		for (int i = 0; i < f.length(); i++) {
			if( ! gültigeFarbe(f.charAt(i))) {
				return false;
			}
		}
		
		return true;
	}
	
	

}
