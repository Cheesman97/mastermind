import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CodeTest {
	private Code c;

	/*@Test
	void testVersuch() {
		c = new Code();
		
	}

	@Test
	void testGewonnen() {
		fail("Not yet implemented");
	}

	@Test
	void testGültigeFarbeChar() {
		fail("Not yet implemented");
	}*/

	@Test
	public void testGültigeFarbeString() {
		c = new Code();
		assertTrue(c.gültigeFarbe("rgvb"));
		assertFalse(c.gültigeFarbe("ayay"));
	}

}
