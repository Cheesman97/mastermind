import java.util.Scanner;

public class Konsole {	
	Scanner scanner = new Scanner(System.in);

	public String eingabelesen() {
		System.out.println("Versuch eingeben (4 Zeichen): ");
		String eingabe = scanner.nextLine();
		//scanner.close();
		return eingabe;
	}
	
	public boolean eingabepruefen(String eingabe) {
		if (eingabe.length() != 4) {
			System.out.println("Die Eingabe darf nur 4 Zeichen enthalten");
			return false;
		}
		else {
			
			return Code.gültigeFarbe(eingabe);
			
		}
	}
}
