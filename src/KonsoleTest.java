import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class KonsoleTest {
	private Konsole k;
	
	@Test
	public void EingabeZuWenigZeichen() {
		k = new Konsole(); 
		assertFalse(k.eingabepruefen("rgb"));
	}
	
	@Test
	public void EingabeZuVielZeichen() {
		k = new Konsole(); 
		assertFalse(k.eingabepruefen("rgbbb"));
	}
	
	@Test
	public void EingabeOk() {
		k = new Konsole(); 
		assertTrue(k.eingabepruefen("rgbb"));
	}

}
