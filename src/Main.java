import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Code a = new Code();
		Konsole k = new Konsole();
		
		System.out.print("Farben: ");
		
		for(int i = 0; i < a.farben.length; i ++) {
			System.out.print(a.farben[i] + " ");
		}
		System.out.println();
		String eingabe;
		String check;
		int versuche = 10;
		do {
			do {
				try {
					eingabe = k.eingabelesen();
				}catch(InputMismatchException e){
					eingabe = k.eingabelesen();
				}
				
			}while( ! k.eingabepruefen(eingabe));
			check = a.versuch(eingabe);
			versuche --;
			System.out.println("Anzahl verbleibende Versuche: "+versuche);
		}while( ! a.gewonnen(check) && versuche >= 1);
		
		System.out.print("Code war: ");
		
		for(int i = 0; i < a.code.length; i ++) {
			System.out.print(a.farben[a.code[i]] + " ");
		}
		
	
	}

}
